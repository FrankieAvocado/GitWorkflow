var webpack = require("webpack");

var config = {
    entry: "./webapp/app/index.js",
    output: {
        path: "./webapp/dist",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test : /\.(js|jsx)?/,
                exclude: "node_modules",
                loader : "babel-loader",
                query: {
                    presets: ["es2015", "react"],
                    plugins: ["transform-object-rest-spread"]
                }
            },
            {
                test: /\.scss$/,
                exclude: "node_modules",
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                exclude: "node_modules",
                loader: "file-loader?name=/content/[name].[ext]"
            },
            {
                test: /\.html/,
                exclude: "node_modules",
                loader: "file-loader?name=[name].[ext]"
            },
            {
                test: /\.json/,
                exclude: "node_modules",
                loader: "json-loader"
            }
        ]
    }
};

module.exports = config;