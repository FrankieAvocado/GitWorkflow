/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");
var CodeLine = require("./code-line.jsx");

module.exports = React.createClass({
    displayName: "CodeContainer",
    render: function(){
        var collapseChildren = this.props.collapseChildren;
        var childItems = this.props.examples.map(function(example){
            return (
                <CodeLine key={example.id} line={example.text} collapsed={collapseChildren}></CodeLine>
            )
        });

        return(
            <div className="code-container">
                <ul>
                    {childItems}
                </ul>
            </div>
        );
    }
});