/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");

module.exports = React.createClass({
    displayName: "FriendlyHeader",
    render: function(){
        return(
            <h1>Let's talk about Git and GitHub!</h1>
        );
    }
});