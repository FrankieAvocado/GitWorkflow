/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");
var Section = require("./section.jsx");

module.exports = React.createClass({
    displayName: "SectionList",
    render: function(){

        var internalSectionList = this.props.data.sections.map(function(section){
           return(
             <Section key={section.id} section={section}></Section>
           )
        });

        return(
            <div className="section-list">
                <ul>
                    {
                        internalSectionList
                    }
                </ul>
            </div>
        );
    }
});