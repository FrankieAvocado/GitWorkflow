/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");
var SectionItem = require("./section-item.jsx");

module.exports = React.createClass({
    displayName: "Section",
    render: function(){

        var iconClass = "fa " + this.props.section.icon;
        var childItems = this.props.section.items.map(function(item){
           return (
               <SectionItem key={item.id} item={item}></SectionItem>
           )
        });

        return(
            <li className="section">
                <h2><i className={iconClass}></i> {this.props.section.title}</h2>
                <ul>
                    {childItems}
                </ul>
            </li>
        );
    }
});