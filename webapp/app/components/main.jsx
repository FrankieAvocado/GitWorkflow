/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");
var ReactDOM = require("react-dom");
var FriendlyHeader = require("./friendly-header.jsx");
var SectionList = require("./section-list.jsx");
var allSections = require("../data.json");

module.exports = ReactDOM.render(
    <div className="useful-info">
        <FriendlyHeader></FriendlyHeader>
        <SectionList data={allSections}></SectionList>
    </div>,
    document.getElementById("Content")
);