/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");

module.exports = React.createClass({
    displayName: "CodeLine",
    render: function(){
        var escapedLine = this.props.line.replace(new RegExp(' ', 'g'), '&nbsp;');
        var lineClass = "code-line " + (this.props.collapsed ? "collapsed" : "");
        return(
            <li className={lineClass}>
                <span dangerouslySetInnerHTML={{__html: escapedLine}} />
            </li>
        );
    }
});