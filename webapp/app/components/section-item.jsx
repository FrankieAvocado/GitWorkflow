/**
 * Created by ebates007 on 3/15/2017.
 */
var React = require("react");
var CodeContainer = require("./code-container.jsx");

module.exports = React.createClass({
    displayName: "SectionItem",
    render: function(){

        var examplesList = (this.props.item.examples && this.props.item.examples.length > 0) ?
            (<CodeContainer examples={this.props.item.examples} collapseChildren={this.props.item.collapseExamples}></CodeContainer>) : "";

        return(
            <li className="section-item">
                <h3>{this.props.item.name}</h3>
                <p>{this.props.item.description}</p>
                {examplesList}
            </li>
        );
    }
});