/**
 * Created by ebates007 on 3/16/2017.
 */
var chai = require("chai");
var assert = chai.assert;

describe("This sample test", function(){
    it("should confirm that 2 + 2 is equal to 4", function(){
        // arrange
        var result = 2 + 2;
        // act

        // assert
        assert.equal(4, result);
    })
});