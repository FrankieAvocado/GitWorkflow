var express = require("express");
var environment = process.env.ENVIRONMENT;
var bodyParser = require("body-parser");
var port = process.env.PORT || 8080;
var app = express();

app.use("/", express.static("./webapp/dist"));
app.use(bodyParser.json());

app.listen(port);
console.log("Server is listening on port: " + port);